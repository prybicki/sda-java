package pl.sda.poznan.shop;

import pl.sda.poznan.shop.model.CartItem;
import pl.sda.poznan.shop.model.Product;
import pl.sda.poznan.shop.model.ShopCart;
import pl.sda.poznan.shop.repository.ProductRepository;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        initializeApp();
        boolean exit = false;
        ShopCart cart = new ShopCart();
        while (!exit) {
            printMenu();
            Scanner scanner = new Scanner(System.in);
            int option = scanner.nextInt();
            switch (option) {
                case 1: {
                    System.out.println("List of products: ...");
                    ProductRepository.getInstance()
                            .getAll()
                            .forEach(System.out::println);
//                            .forEach(p -> System.out.println(p.toString()));
                    break;
                }
                case 2: {
                    System.out.println("Give product id");
                    long productId = scanner.nextLong();
                    Product product = ProductRepository.getInstance().getById(productId);
                    System.out.println("Your choice: " + product.toString());
                    System.out.println("Give quantity: ");
                    int quantity = scanner.nextInt();

                    //logic add product to cart
                    // I sposob -- all args ctor
//                    CartItem cartItem =
//                            new CartItem(product.getName(), product.getDescription(), product.getPrice(),
//                                    quantity);

                    // Java Beans method
                    CartItem cartItem1 = new CartItem();
                    cartItem1.setDescription(product.getDescription());
                    cartItem1.setName(product.getName());
                    cartItem1.setUnitPrice(product.getPrice());
                    cartItem1.setQuantity(quantity);

                    // Builder
                    CartItem build = CartItem.builder()
                            .description(product.getDescription())
                            .name(product.getName())
                            .unitPrice(product.getPrice())
                            .quantity(quantity)
                            .build();

                    cart.add(build);
                    System.out.println("Added element to cart.");
                    break;
                }
                case 8: {

                }
                case 9: {
                    System.out.println("Sign in");
                    System.out.println("Login: ");
                    System.out.println("Password: ");
                    break;
                }
                case 10: {
                    System.out.println("Registration");
                    System.out.println("Fill following data");
                    System.out.println("Name: ");
                    System.out.println("Surname: ");
                    System.out.println("Email: ");
                    System.out.println("Password: ");
                    System.out.println("Repeat password: ");
                    break;
                }
                case 0: {
                    System.out.println("Exit program ... ");
                    exit = true;
                }
            }
        }
    }

    private static void printMenu() {
        System.out.println(" ");
        System.out.println("Menu: ");
        System.out.println("1. Show all products");
        System.out.println("2. Add to cart");

        System.out.println("8. Save products to file");
        System.out.println("9. Sign in");
        System.out.println("10. Registration");
        System.out.println("0. Exit");
    }

    private static void initializeApp() {
        seedProducts();
    }

    private static void seedProducts() {
        Product smartphone = new Product("Nokia", "Desc", 500D);
        Product laptop = new Product("Dell", "Ultrabook", 1500D);
        Product console = new Product("Xbox", "One X", 2000.00);

        ProductRepository
                .getInstance()
                .add(Arrays.asList(smartphone, laptop, console));
    }
}
