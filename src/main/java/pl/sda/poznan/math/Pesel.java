package pl.sda.poznan.math;

import java.util.LinkedList;

public class Pesel {

    private int[] parameters = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};

    public boolean validatePesel(String number) {
        if (number.isEmpty() || number.length() != 11) {
            throw new IllegalArgumentException("Length error");
        }
        return calculateControlSum(number) % 10 == convertCharToInt(number.charAt(10));
    }

    private int calculateControlSum(String number) {
        int controlSum = 0;
        for (int i = 0; i < number.length() - 1; i++) {
            controlSum += parameters[i] * convertCharToInt(number.charAt(i));
        }
        return controlSum;
    }

    private int convertCharToInt(char c) {
        return Integer.parseInt(String.valueOf(c));
    }
}
