package pl.sda.poznan.math;

public class Factorial {

    private Factorial() {
        throw new IllegalStateException("Utility class");
    }

    public static int factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n cannot be less than zero");
        }
        if (n == 0 || n == 1) {
            return 1;
        }
        return n * factorial(n - 1);

    }
}
