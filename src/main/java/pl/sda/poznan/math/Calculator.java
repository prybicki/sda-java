package pl.sda.poznan.math;

public class Calculator {
    public static int sum(String str) {
        if (str.isEmpty()) {
            return 0;
        } else if (str.length() == 1) {
            return Integer.parseInt(str);
        } else {
            String[] split = str.split(",");
            int sum = 0;
            for (int i = 0; i < split.length; i++) {
                sum = sum + parseInteger(split[i]);
            }
            return sum;
        }
    }

    private static int parseInteger(String s) {
        return Integer.parseInt(s);
    }
}
