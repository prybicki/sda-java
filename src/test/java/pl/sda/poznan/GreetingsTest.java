package pl.sda.poznan;

import org.junit.Test;

import static org.junit.Assert.*;

public class GreetingsTest {

    @Test
    public void should_greet_person() {
        String greet = Greetings.greet("Jan");
        assertEquals("Hello, Jan", greet);
    }

    @Test
    public void should_greet_anyone() {
        String greet = Greetings.greet(null);
        assertEquals("Hello, my friend", greet);
    }

    @Test
    public void should_greet_with_upper_case() {
        String greet = Greetings.greet("JAN");
        assertEquals("HELLO, JAN", greet);
    }

    @Test
    public void should_greet_two_persons() {
        String greet = Greetings.greet("Jan", "Ala");
        assertEquals("Hello, Jan and Ala", greet);
    }

    @Test
    public void should_greet_multiple_persons() {
        String greet = Greetings.greet("Piotr", "Anna", "Paulina");
        assertEquals("Hello, Piotr, Anna and Paulina",greet);
    }

    @Test
    public void should_greet_with_capital_letters() {
        String greet = Greetings.greet("Jan", "PIOTR", "Magda");
        assertEquals("Hello, Jan and Magda. AND HELLO PIOTR", greet);
    }

}