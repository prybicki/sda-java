package pl.sda.poznan.math;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FactorialTest {

    @Before
    public void setUp() {
        System.out.println("Wykonywane przed testem");
    }

    private int number;
    private int result;

    public FactorialTest(int number, int result){
        this.number = number;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {1,1},
                {2,2},
                {3,6},
                {4,24},
                {5,120},
                {6,720}
        });
    }

    @Test
    public void should_check_factorial() {
        assertEquals(Factorial.factorial(number),result);
    }

    @Test (expected = IllegalArgumentException.class)
    public void should_throw_exception_on_negative_value() {
        int n = -5;
        int result = Factorial.factorial(n);
    }

}