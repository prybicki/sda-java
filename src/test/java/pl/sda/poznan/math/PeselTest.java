package pl.sda.poznan.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PeselTest {

    private String number;
    private boolean isValid;

    public PeselTest(String number, boolean isValid) {
        this.number = number;
        this.isValid = isValid;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"47092006053", true},
                {"28082411958", true},
                {"83759293821", false}
        });
    }

    @Test
    public void should_check_pesel_is_correct() {
        Pesel validator = new Pesel();
        boolean validate = validator.validatePesel(number);
        assertEquals(this.isValid,validate);

    }
}