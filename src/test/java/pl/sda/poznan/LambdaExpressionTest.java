package pl.sda.poznan;

import org.junit.Before;
import org.junit.Test;
import pl.sda.poznan.shop.model.Product;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class LambdaExpressionTest {

    private List<Product> products;

    @Before
    public void init() {
        Product smartphone = new Product("Nokia", "Desc", 500D);
        Product laptop = new Product("Dell", "Ultrabook", 1500D);
        Product console = new Product("Xbox", "One X", 2000.00);

        this.products = Arrays.asList(smartphone, laptop, console);
    }

    //playground
    @Test
    public void test() {
        //znajdz produkty drozsze niz 1000
        List<Product> result = products
                .stream()
                .filter(product -> product.getPrice() > 1000)
                .collect(Collectors.toList());

        assertEquals(2, result.size());
        assertEquals(3, products.size());
    }

    @Test
    public void should_find_where_starts_with_given_letter() {
        //wyrazenie filtrujace, gdzie mamy produkty drozsze niz 1000 i zaczynajace sie od X
        List<Product> result = products
                .stream()
                .filter(p -> p.getPrice() > 1000)
                .filter(p -> p.getName().startsWith("X"))
                .collect(Collectors.toList());

        assertEquals(1, result.size());
    }

    //znajdz pierwszy tanszy niz 2000
    @Test
    public void should_find_first_cheaper_than_2000() {
        //Optional opakowuje obiekty
        Optional<Product> optionalProduct = products
                .stream()
                .filter(p -> p.getPrice() > 100)
                .findFirst();

        Product p = optionalProduct.orElseThrow(() -> new NoSuchElementException());
    }

    //posortuj alfabetycznie po nazwie
    @Test
    public void should_sort_list_in_alphabetical_order() {
        this.products.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));
        //collect.sort(((o1, o2) -> o1.getPrice().compareTo(o2.getPrice()));
        //to samo z uzyciem method reference
        this.products.sort(Comparator.comparing(Product::getName));
    }

    //dla kazdego produktu pobierz jego nazwe na liste
    @Test
    public void should_map_to_names() {
        List<String> list = this.products
                .stream()
                .map(p -> p.getName())
                .collect(Collectors.toList()); //metoda collect mowi ze skonczylismy definiowanie łańcuch i zapisuje dane do listy
        assertEquals(3,list.size());
    }
}